<?php
require_once( 'lab/affari.php' );
require_once( 'lab/affari-admin-style.php' );
require_once( 'lab/email-logged.php' );
require_once( 'lab/lab-functions.php' );
require_once( 'lab/login-logout-menu.php' );
require_once( 'lab/related-functions.php' );

function affari_init() {
	// let's get language support going, if you need it
	load_theme_textdomain( 'affari', get_template_directory() . '/library/translation' );

	// launching operation cleanup
	add_action( 'init', 'affari_head_cleanup' );
	// A better title
	add_filter( 'wp_title', 'rw_title', 10, 3 );
	// remove WP version from RSS
	add_filter( 'the_generator', 'affari_rss_version' );
	// remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'affari_remove_wp_widget_recent_comments_style', 1 );
	// clean up comment styles in the head
	add_action( 'wp_head', 'affari_remove_recent_comments_style', 1 );
	// enqueue base scripts and styles
	add_action( 'wp_enqueue_scripts', 'affari_scripts_and_styles', 999 );
	// ie conditional wrapper

	affari_custom_image_sizes();

	affari_theme_support();
}

// let's get this party started
add_action( 'after_setup_theme', 'affari_init' );

/**
 * Custom functions
 */
register_nav_menus( array(
	'top_links' => 'Top Links',
) );

add_filter('xmlrpc_enabled', '__return_false');

?>
