<?php
if (!is_user_logged_in() ) {
wp_redirect ( home_url("/portfolio-login") );
exit;
}
?>

<?php
/**
 * Template Name: Hub Page (welcome page)
 *
 * @package affari lab wp theme
 * @subpackage Affari lab contents
 * @since affari dev team
 */

get_header();

if ( get_field('has_hero_image') ) {
	affari_display_hero_section();
}

if ( get_field('has_hero_video') ) {
	affari_display_hero_video();
}

affari_display_flexible_content();

get_footer();

?>
