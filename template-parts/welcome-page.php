<section class="content" id="welcome-page">
  <?php
  $current_user = wp_get_current_user();
  ?>

    <div class="inner full">

        <div class="content-wrapper">
            <div class="content">
              <h1 align"center"> https://affarilab.com<?php echo $_SERVER['REQUEST_URI'] ?></h1>
              <p><?php  echo 'Hello ' . $current_user->user_firstname . ' ' . $current_user->user_lastname . ',' . '<br />'; ?></p>
              <?php the_sub_field( 'welcome_message' ); ?>
            </div>
        </div>

    </div>

</section>
