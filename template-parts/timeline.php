<section id="timeline">
      <?php if( have_rows('timeline') ): $counter = 0; ?>

      	<div class="timeline">
        	<?php while( have_rows('timeline') ): the_row(); $counter++;
        		$year  = get_sub_field('timeline_year');
            $image = get_sub_field('timeline_image');
        		$text  = get_sub_field('timeline_text');
        		?>
            <?php if ( get_row_index() % 2 == 0) : ?>
                <div class="container right">
                  <div class="content">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                    <h3><?php echo $year; ?></h3>
                    <p><?php echo $text; ?></p>
                  </div>
                </div>
            	<?php else: ?>
                <div class="container left">
                  <div class="content">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                    <h3><?php echo $year; ?></h3>
                    <p><?php echo $text; ?></p>
                  </div>
                </div>
            	<?php endif; ?>

        	<?php endwhile; ?>
        </div>
      <?php endif; ?>
</section>
