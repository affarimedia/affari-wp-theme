<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php wp_title(''); ?></title>

    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
    <!--[if IE]>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <![endif]-->
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>

    <?php // drop Google Analytics Here ?>

    <?php // end analytics ?>

</head>

<body <?php body_class(); ?> ">

    <header class="header">

        <nav class="nav" id="menu" role="navigation">

            <?php
                wp_nav_menu( array(
                    'container_class' => 'Main Menu',
                ) );
            ?>

        </nav>

    </header>
    <div class="clear"></div>
