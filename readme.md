# Affari Wordpress Theme
## Wordpress for \client-name-here/

* webpack 4 with loaders and plugins
* bootstrap 4 + popper.js with sass compiler
* font-awesome free
* jQuery + jQuery easing
* Affari scrolling bar and stick nav JS
* GreenSock 'gsap' JS
* flickity carousel
* WOW JS
* animate.css
* modernizr
* Babel ES6 Compiler
* create gzip css and js = increase performance on Browser


### start

----------------------------------------
setup
----------------------------------------
Install Dependencies

go to your new theme project folder: open terminal and type:
```
git clone https://bitbucket.org/affarimedia/xxx.git
```

- delete folder .git;
- install npm:
```
npm i
```

Open webpack.config.js and change the Browser Sync proxy (line 4) to the dev URL of your choice (Example: http://affari.local/)


----------------------------------------
# Scripts / Commands
----------------------------------------

runs build command to compile SCSS and Javascript
```
npm run build
```

runs the watch command for changes in the SCSS and Javascript files then compiles them.
```
npm run watch
```


----------------------------------------
!important
----------------------------------------
1) install npm global
```
npm install -g
```

2) install on project folder the composer
```
php composer.phar install
```
-- Composer is a tool for dependency management in PHP. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you


### more details
* Add your HTML files by inserting or including them in the 'src' directory.
* Make sure you restart development server after adding new HTML files
```
npm start
```

* Add images to your 'src/media' folder.
* Add sass (.scss) files to 'src/scss', named then like '_custom.scss' and import then in 'main.scss'
```
@import "sassfilename";
```


----------------------------------------
Licenses & Credits
----------------------------------------
- WebPack: https://webpack.js.org (MIT - https://webpack.js.org/license/)
- GZIP Compression: https://www.gnu.org/software/gzip (GNU)
- Babel ES 6: https://babeljs.io/ (MIT https://github.com/babel/babel/blob/master/LICENSE)
- all declarative formats for setup automation in this app/theme is inspire on 12factor: https://github.com/heroku/12factor/ (MIT)
- Bootstrap: http://getbootstrap.com | https://github.com/twbs/bootstrap/blob/master/LICENSE (Code licensed under MIT documentation under CC BY 3.0.)
- jQuery: https://jquery.org | (Code licensed under MIT)
- Font Awesome: http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
- Flickity: https://flickity.metafizzy.co/license.html (GPLv3)
- GreenSock JS: https://greensock.com/standard-license
- WOW JS: https://wowjs.uk (100% MIT ;) https://raw.githubusercontent.com/graingert/WOW/master/LICENSE)
- Animate.css: https://github.com/daneden/animate.css/blob/master/LICENSE (MIT)
- Modernizr: https://modernizr.com/ (Modernizr 3.5.0 | MIT)


# additional notes
----------------------------------------
custom admin style
----------------------------------------
on login folder, insert your logo as 'logo.png' and your background image as 'Surface.webp'

                      === Affari Theme for Wordpress ===
