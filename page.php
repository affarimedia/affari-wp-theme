<?php
get_header();

if ( get_field('has_hero_image') ) {
	affari_display_hero_section();
}

if ( get_field('has_hero_video') ) {
	affari_display_hero_video();
}

affari_display_flexible_content();

get_footer();

?>
