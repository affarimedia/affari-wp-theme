<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

/* Load up the language */
function lg_load_textdomain() {
  $path = basename(dirname(__FILE__)) . '/';

  load_plugin_textdomain('lg', false, $path);
}
add_action('plugins_loaded', 'lg_load_textdomain');

/* Add a metabox in admin menu page */
function lg_add_nav_menu_metabox() {
  add_meta_box('lg', __('Login/Logout', 'lg'), 'lg_nav_menu_metabox', 'nav-menus', 'side', 'default');
}
add_action('admin_head-nav-menus.php', 'lg_add_nav_menu_metabox');

/* The metabox code : Awesome code stolen from screenfeed.fr (GregLone) Thank you mate. */
function lg_nav_menu_metabox($object) {
  global $nav_menu_selected_id;

  $elems = array(
    '#lglogin#' => __('Log In', 'lg'),
    '#lglogout#' => __('Log Out', 'lg'),
    '#lgloginout#' => __('Log In', 'lg').'|'.__('Log Out', 'lg')
  );

  class lgLogItems {
    public $db_id = 0;
    public $object = 'lglog';
    public $object_id;
    public $menu_item_parent = 0;
    public $type = 'custom';
    public $title;
    public $url;
    public $target = '';
    public $attr_title = '';
    public $classes = array();
    public $xfn = '';
  }

  $elems_obj = array();

  foreach($elems as $value => $title) {
    $elems_obj[$title]              = new lgLogItems();
    $elems_obj[$title]->object_id		= esc_attr($value);
    $elems_obj[$title]->title			  = esc_attr($title);
    $elems_obj[$title]->url			    = esc_attr($value);
  }

  $walker = new Walker_Nav_Menu_Checklist(array());

  ?>
  <div id="login-links" class="loginlinksdiv">
    <div id="tabs-panel-login-links-all" class="tabs-panel tabs-panel-view-all tabs-panel-active">
      <ul id="login-linkschecklist" class="list:login-links categorychecklist form-no-clear">
        <?php echo walk_nav_menu_tree(array_map('wp_setup_nav_menu_item', $elems_obj), 0, (object) array('walker' => $walker)); ?>
      </ul>
    </div>
    <p class="button-controls">
      <span class="add-to-menu">
        <input type="submit"<?php disabled($nav_menu_selected_id, 0); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e('Add to Menu', 'lg'); ?>" name="add-login-links-menu-item" id="submit-login-links" />
        <span class="spinner"></span>
      </span>
    </p>
  </div>
  <?php
}

/* Modify the "type_label" */
function lg_nav_menu_type_label($menu_item) {
  $elems = array('#lglogin#', '#lglogout#', '#lgloginout#');
  if(isset($menu_item->object, $menu_item->url) && 'custom' == $menu_item->object && in_array($menu_item->url, $elems)) {
    $menu_item->type_label = __('Dynamic Link', 'lg');
  }

  return $menu_item;
}
add_filter('wp_setup_nav_menu_item', 'lg_nav_menu_type_label');

/* Used to return the correct title for the double login/logout menu item */
function lg_loginout_title($title) {
	$titles = explode('|', $title);

	if(!is_user_logged_in()) {
		return esc_html(isset($titles[0])?$titles[0]:__('Log In', 'lg'));
	} else {
		return esc_html(isset($titles[1]) ? $titles[1] : __('Log Out', 'lg'));
	}
}

/* The main code, this replace the #keyword# by the correct links with nonce ect */
function lg_setup_nav_menu_item($item) {
	global $pagenow;

	if($pagenow != 'nav-menus.php' && !defined('DOING_AJAX') && isset($item->url) && strstr($item->url, '#lg') != '') {
		$login_page_url       = get_option('lg_login_page_url', wp_login_url());
    $logout_redirect_url  = get_option('lg_logout_redirect_url', home_url());

		switch($item->url) {
			case '#lglogin#':
        $item->url = $login_page_url;
        break;
			case '#lglogout#':
        $item->url = wp_logout_url($logout_redirect_url);
        break;
			default: //Should be #lgloginout#
        $item->url = (is_user_logged_in()) ? wp_logout_url($logout_redirect_url) : $login_page_url;
        $item->title = lg_loginout_title($item->title);
		}
	}

	return $item;
}
add_filter('wp_setup_nav_menu_item', 'lg_setup_nav_menu_item');

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}

    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}


function lg_settings_page() {
  $login_page_url       = get_option('lg_login_page_url', wp_login_url());
  $logout_redirect_url  = get_option('lg_logout_redirect_url', home_url());
  ?>
    <div class="wrap">
      <div class="icon32"></div>
      <h2><?php _e('', 'lg'); ?></h2>
      <div class="lg_spacer" style="height:25px;"></div>

      <?php if(isset($_GET['lgsaved'])): ?>
        <div id="message" class="updated notice notice-success is-dismissible below-h2">
          <p><?php _e('Settings saved...', 'lg'); ?></p>
        </div>
      <?php endif; ?>

      <form action="" method="post">
        <label for="lg_login_page_url"><?php _e('Login Page URL', 'lg'); ?></label><br/>
        <small><?php _e(''); ?></small><br/>
        <input type="text" id="lg_login_page_url" name="lg_login_page_url" value="<?php echo $login_page_url; ?>" style="min-width:250px;width:60%;" /><br/><br/>

        <label for="lg_logout_redirect_url"><?php _e('Logout Redirect URL', 'lg'); ?></label><br/>
        <small><?php _e(''); ?></small><br/>
        <input type="text" id="lg_logout_redirect_url" name="lg_logout_redirect_url" value="<?php echo $logout_redirect_url; ?>" style="min-width:250px;width:60%;" /><br/><br/>

        <input type="submit" id="lg_settings_submit" name="lg_settings_submit" value="<?php _e('Save Settings', 'lg'); ?>" class="button button-primary" />
      </form>
    </div>
  <?php
}

function lg_setup_menus() {
  add_options_page('lg Settings', 'Login URLs', 'manage_options', 'lg-settings', 'lg_settings_page');
}
add_action('admin_menu', 'lg_setup_menus');

function lg_save_settings() {
  if(isset($_POST['lg_settings_submit'])) {
    $login_page_url       = (isset($_POST['lg_login_page_url']) && !empty($_POST['lg_login_page_url'])) ? $_POST['lg_login_page_url'] : wp_login_url();
    $logout_redirect_url  = (isset($_POST['lg_logout_redirect_url']) && !empty($_POST['lg_logout_redirect_url'])) ? $_POST['lg_logout_redirect_url'] : home_url();

    update_option('lg_login_page_url', esc_url_raw($login_page_url));
    update_option('lg_logout_redirect_url', esc_url_raw($logout_redirect_url));

    wp_redirect($_SERVER['REQUEST_URI']."&lgsaved=true");
    die();
  }
}
add_action('admin_init', 'lg_save_settings');

// END login or logout end

?>
