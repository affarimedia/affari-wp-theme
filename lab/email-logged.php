<?php

// change username or Email address message on login screen
add_filter( 'gettext', 'register_text' );
add_filter( 'ngettext', 'register_text' );
function register_text( $translated ) {
    $translated = str_ireplace(
        'Username or Email Address',
        'Email Address',
        $translated
    );
    return $translated;
}

//global constants
define('af_VERSION', '');
define('af_URL', plugin_dir_url(__FILE__));

//Go Go Go
new af_Settings();
new af_Engine();

class af_Engine{

    /**
     * Main Constructor
     */
    public function __construct() {

        //Process Login
        if(af_Settings::isLoginSmart()){
            //remove wordpress authentication
            remove_filter('authenticate', 'wp_authenticate_username_password', 20);

            //custom authentication function
            add_filter('authenticate', array($this, 'callback_Authenticate'), 20, 3);

            //Process Gettext for custom strings
            add_action('login_form_login', array($this,'callback_LoginFormLogin'));
        }


        //Process Register
        if(af_Settings::isRegistrationSmart()){
            //Assign email to username
            add_action('login_form_register', array($this, 'callback_LoginFormRegister'));

            //Remove error for username, show error for email only.
            add_filter('registration_errors', array($this, 'callback_RegistrationErrors'), 10, 3);

            /**
             * Add customization to registration form.
             * Ref:
             * http://codex.wordpress.org/Customizing_the_Login_Form
             * http://codex.wordpress.org/Function_Reference/wp_enqueue_script
             *
             */
            //>3.0.1 only
            //add_action('login_enqueue_scripts', array($this, 'afLoginEnqueueScripts'));
            add_action('login_head', array($this, 'callback_LoginHead'));
            add_action('login_footer', array($this, 'callback_LoginFooter'));
        }


        //Process Retrieve Password
        if(af_Settings::isRetrievePasswordSmart()){
            add_action('login_form_lostpassword', array($this, 'callback_LoginFormLostPassword'));
            add_action('login_form_retrievepassword', array($this, 'callback_LoginFormLostPassword'));
        }
    }

    ############################################################################
    #  Login With Email
    ############################################################################

    /**
     * Custom Authentication function
     */
    public function callback_Authenticate($user, $email, $password){

        //Check for empty fields
        if(empty($email) || empty ($password)){
            //create new error object and add errors to it.
            $error = new WP_Error();

            /*
             * Added v1.0
             * Don't know why, but WP doesn't show an error when both fields are empty.
             * Thats why we are making it smart.
             */
            if('post' === strtolower($_SERVER['REQUEST_METHOD'])){
                if(empty($email) && empty($password)){//Both fields are empty.
                    $error->add('empty_username', af_Settings::getString(af_Settings::STRING_LOG_EMPTY_BOTH_FIELDS) );
                    return $error;
                }
            }

            if(empty($email)){ //No email
                $error->add('empty_username', af_Settings::getString(af_Settings::STRING_LOG_EMPTY_EMAIL));
            }
            else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //Invalid Email
                $error->add('invalid_username', af_Settings::getString(af_Settings::STRING_LOG_INVALID_EMAIL));
            }

            if(empty($password)){ //No password
                $error->add('empty_password', af_Settings::getString(af_Settings::STRING_LOG_EMPTY_PASSWORD));
            }

            return $error;
        }

        //Check if user exists in WordPress database
        $user = get_user_by('email', $email);

        //bad email
        if(!$user){
            $error = new WP_Error();
            $error->add('invalid', af_Settings::getString(af_Settings::STRING_LOG_INVALID_EMAIL_PASSWORD));
            return $error;
        }
        else{ //check password
            if(!wp_check_password($password, $user->user_pass, $user->ID)){ //bad password
                $error = new WP_Error();
                $error->add('invalid', af_Settings::getString(af_Settings::STRING_LOG_INVALID_EMAIL_PASSWORD));
                return $error;
            }else{
                return $user; //passed
            }
        }
    }

    /**
     * Return custom string message
     */
    public function callback_LoginFormLogin(){
        //Change "Username" text to "Email"
        add_filter('gettext', array($this, 'callback_LoginFormGettext'), 20, 3);
    }

    public function callback_LoginFormGettext($translated_text, $text, $domain ){
        switch($translated_text){
            case 'Username': //For Login
                $translated_text = 'Email';
                break;
            case 'Check your e-mail for the confirmation link.':
                $translated_text = af_Settings::getString(af_Settings::STRING_RP_LOG_CHECK_EMAIL);
                break;
            case 'Registration complete. Please check your e-mail.':
                $translated_text = af_Settings::getString(af_Settings::STRING_REG_LOG_REGISTRATION_COMPLETE);
                break;
        }

        return $translated_text;
    }



    ############################################################################
    #  Register With Email
    ############################################################################

    /**
     * Hook to registration system,
     * Now the tweak goes here.
     * Username: As WP registration requires username, we need to provide a username while
     * registering. So we assign local part of email as username, ex: demo#demo@example.com
     * and username would be demodemo (no special chars).
     *
     * Duplicate Username: In case username already exists, system tries to change
     * username by adding a random number as suffix. Random number is between
     * 1 to 999. Ex: demodemo_567
     */
    public function callback_LoginFormRegister(){
        if(isset($_POST['user_login']) && isset($_POST['user_email']) && !empty($_POST['user_email'])){
            //In case user email contains single quote ', WP will add a slash automatically. Yes, emails can use special chars, see RFC 5322
            $_POST['user_email'] = stripslashes($_POST['user_email']);

            // Split out the local and domain parts
            list( $local, ) = explode( '@', $_POST['user_email'], 2 );

            //Sanitize special characters in email fields, if any. Yes, emails can use special chars, see RFC 5322
            $_POST['user_login'] = sanitize_user($local, true);

            $pre_change = $_POST['user_login'];
            //In case username already exists, change it
            while(username_exists($_POST['user_login'])){
                $_POST['user_login'] = $pre_change . '_' . rand(1, 999);
            }
        }

        //Change Registration related text
        add_filter('gettext', array($this, 'callback_RegisterGettext'), 20, 3);
    }

    /**
     * Remove registration message for username
     */
    public function callback_RegistrationErrors($wp_error, $sanitized_user_login, $user_email){
        if(isset($wp_error->errors['empty_username'])){
            unset($wp_error->errors['empty_username']);
        }

        if(isset($wp_error->errors['username_exists'])){
            unset($wp_error->errors['username_exists']);
        }
        return $wp_error;
    }

    /**
     * Bit tweaking to hide username field
     */
    public function callback_LoginHead(){
        //Don't show username field in register form.
        ?>
            <style>
                #registerform > p:first-child{
                    visibility: hidden;
                    display:none;
                }
            </style>
        <?php
    }

    /**
     * Just a backup to remove username field, although css is suffice
     */
    public function callback_LoginFooter(){
        ?>
            <script type="text/javascript">
                try{
                    var af_username_p = document.getElementById('registerform').children[0];
                    af_username_p.style.display = 'none';
                }catch(e){}

                //Focus email
                try{document.getElementById('user_email').focus();}catch(e){}
            </script>
        <?php

    }

    public function callback_RegisterGettext($translated_text, $text, $domain ){
        switch($translated_text){
            case '<strong>ERROR</strong>: Please type your e-mail address.':
                $translated_text = af_Settings::getString(af_Settings::STRING_REG_EMPTY_EMAIL);
                break;
            case '<strong>ERROR</strong>: The email address isn&#8217;t correct.':
                $translated_text = af_Settings::getString(af_Settings::STRING_REG_INVALID_EMAIL);
                break;
            case '<strong>ERROR</strong>: This email is already registered, please choose another one.':
                $translated_text = af_Settings::getString(af_Settings::STRING_REG_REGISTERED_EMAIL);
                break;
        }

        return $translated_text;
    }


    ############################################################################
    #  Reset Password With Email
    ############################################################################

    public function callback_LoginFormLostPassword(){
        if('post' == strtolower($_SERVER['REQUEST_METHOD']) && isset($_POST['user_login'])){

            //To skip default wordpress processing.
            $_SERVER['REQUEST_METHOD'] = ':(';
            global $errors;

            if(empty($_POST['user_login'])){
                $errors->errors['empty_username'] = array(af_Settings::getString(af_Settings::STRING_RP_EMPTY_EMAIL));

                //In case of error, later restore previous REQUEST_METHOD value
                add_action('lost_password', array($this, 'callback_LostPassword'));
            }else if(!filter_var($_POST['user_login'], FILTER_VALIDATE_EMAIL)){
                $errors->errors['invalid_combo'] = array(af_Settings::getString(af_Settings::STRING_RP_INVALID_EMAIL));

                //In case of error, later restore previous REQUEST_METHOD value
                add_action('lost_password', array($this, 'callback_LostPassword'));
            }else{ //Don't skip now
                $_SERVER['REQUEST_METHOD'] = 'POST';
            }
        }

        //Change "Retrieve Password" related text
        add_filter('gettext', array($this, 'callback_LostPasswordGettext'), 20, 3);
    }

    public function callback_LostPassword(){
        //Restore right value
        $_SERVER['REQUEST_METHOD'] = 'POST';
    }

    public function callback_LostPasswordGettext($translated_text, $text, $domain ){
        switch($translated_text){
            case 'Please enter your username or e-mail address. You will receive a new password via e-mail.':
            case 'Please enter your username or email address. You will receive a link to create a new password via email.':
                $translated_text = af_Settings::getString(af_Settings::STRING_RP_NOTIFICATION);
                //$translated_text = 'Please enter your email address. You will receive a link to create a new password via email.';
                break;
            case '<strong>ERROR</strong>: There is no user registered with that email address.':
                $translated_text = af_Settings::getString(af_Settings::STRING_RP_NO_USER_REGISTERED);
                break;
            case 'Username or E-mail:':
                $translated_text = __('E-mail:', 'login-af-email');
                break;
        }

        return $translated_text;
    }
}

/*
 * Overrides WordPress functionality
 */


############################################################################
#  Override this function from pluggable.php. Now we can send email address
#  with registration message.
############################################################################
if ( !function_exists('wp_new_user_notification') ) :
/**
 * Email login credentials to a newly-registered user.
 *
 * A new user registration notification is also sent to admin email.
 *
 * @since 2.0.0
 *
 * @param int    $user_id        User ID.
 * @param string $plaintext_pass Optional. The user's plaintext password. Default empty.
 */
function wp_new_user_notification($user_id, $plaintext_pass = '') {
    $user = get_userdata( $user_id );

    // The blogname option is escaped with esc_html on the way into the database in sanitize_option
    // we want to reverse this for the plain text arena of emails.
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n";
    $message .= sprintf(__('E-mail: %s'), $user->user_email) . "\r\n";

    @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);

    if ( empty($plaintext_pass) )
            return;

    $message  = sprintf(__('Username: %s'), $user->user_login) . "\r\n";
    $message .= sprintf(__('E-mail: %s'), $user->user_email) . "\r\n";
    $message .= sprintf(__('Password: %s'), $plaintext_pass) . "\r\n";
    $message .= wp_login_url() . "\r\n";

    wp_mail($user->user_email, sprintf(__('[%s] Your username and password'), $blogname), $message);
}
endif;

/**
 * Options for Login with email
 *
 */


//No direct access allowed.
if(!function_exists('add_action')){
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    echo 'Get Well Soon. :)';
    exit();
}

class af_Settings {

    private static $settings;

    /**
     * Constants for retrieveing custom strings
     */
    //Login
    const STRING_LOG_EMPTY_BOTH_FIELDS = 'af_string_log_empty_both_fields';
    const STRING_LOG_EMPTY_EMAIL = 'af_string_log_empty_email';
    const STRING_LOG_INVALID_EMAIL = 'af_string_log_invalid_email';
    const STRING_LOG_EMPTY_PASSWORD = 'af_string_log_empty_password';
    const STRING_LOG_INVALID_EMAIL_PASSWORD = 'af_string_log_invalid_email_password';

    //Register
    const STRING_REG_EMPTY_EMAIL = 'af_string_reg_empty_email';
    const STRING_REG_INVALID_EMAIL = 'af_string_reg_invalid_email';
    const STRING_REG_REGISTERED_EMAIL = 'af_string_reg_registered_email';
    const STRING_REG_LOG_REGISTRATION_COMPLETE = 'af_reg_log_registration_complete';

    //Retrieve Password
    const STRING_RP_EMPTY_EMAIL = 'af_string_rp_empty_email';
    const STRING_RP_NO_USER_REGISTERED = 'af_string_rp_no_user_registered';
    const STRING_RP_INVALID_EMAIL = 'af_string_rp_invalid_email';
    const STRING_RP_NOTIFICATION = 'af_string_rp_notification';
    const STRING_RP_LOG_CHECK_EMAIL = 'af_string_rp_log_check_email';

    public function __construct() {

        self::$settings = $this->initializeSettings();

        //Load CSS
        add_action('admin_enqueue_scripts', array($this, 'adminEnqueueStyle'));
        //Add settings menu
        add_action('admin_menu', array($this, 'constructMenu'));
        //Register Settings
        add_action('admin_init', array($this, 'registerSettings'));
    }
    /**
     * Creates default settings, if no settings exist at all.
     * @return array af_settings
     */
    private function initializeSettings(){

        //Retrieve settings, for updation or returning back.
        $af_settings = get_option('af_settings');

        if(false === $af_settings){ //create default settings

                // no 1.0, create default settings
                if(af_VERSION != get_option('af_version')){

                    update_option('af_version', af_VERSION);
                }

                //default message set
                $af_settings = array(
                    self::STRING_LOG_EMPTY_BOTH_FIELDS                      => '<strong><i>Error</i></strong>: Both fields are required.',
                    self::STRING_LOG_EMPTY_EMAIL                            => '<strong><i>Error</i></strong>: The email field is empty.',
                    self::STRING_LOG_INVALID_EMAIL                          => '<strong><i>Error</i></strong>: The email you entered is invalid.',
                    self::STRING_LOG_EMPTY_PASSWORD                         => '<strong><i>Error</i></strong>: The password field is empty.',
                    self::STRING_LOG_INVALID_EMAIL_PASSWORD                 => '<strong><i>Error</i></strong>: Either the email or password you entered is invalid.',
                    self::STRING_REG_EMPTY_EMAIL                            => '<strong><i>Error</i></strong>: Please type your e-mail address.',
                    self::STRING_REG_INVALID_EMAIL                          => '<strong><i>Error</i></strong>: The email address isn’t correct.',
                    self::STRING_REG_REGISTERED_EMAIL                       => '<strong><i>Error</i></strong>: This email is already registered, please choose another one.',
                    self::STRING_REG_LOG_REGISTRATION_COMPLETE              => 'Registration complete. Please check your e-mail.',
                    self::STRING_RP_EMPTY_EMAIL                             => '<strong><i>Error</i></strong>: You must provide your email.',
                    self::STRING_RP_NO_USER_REGISTERED                      => '<strong><i>Error</i></strong>: No user is registered with given email.',
                    self::STRING_RP_INVALID_EMAIL                           => '<strong><i>Error</i></strong>: Email is invalid.',
                    self::STRING_RP_NOTIFICATION                            => '<strong><i>Error</i></strong>: Please enter your email address. You will receive a link to create a new password via email.',
                    self::STRING_RP_LOG_CHECK_EMAIL                         => 'Check your e-mail for the confirmation link.',
                );

                //compatibility with old 0.9, check prev. stored options.
                $af_0_9['af_l'] = get_option('af_l');
                $af_0_9['af_r'] = get_option('af_r');
                $af_0_9['af_rp'] = get_option('af_rp');

                //no 0.9 options found
                if(!$af_0_9['af_l'] &&
                    !$af_0_9['af_r'] &&
                    !$af_0_9['af_rp'] ){
                    $af_settings['af_enable_login'] = 'on';
                    $af_settings['af_enable_registration'] = 'on';
                    $af_settings['af_enable_retrieve_password'] = 'on';
                }else{ //0.9 options are there.
                    if($af_0_9['af_l']){
                        $af_settings['af_enable_login'] = 'on';
                    }

                    if($af_0_9['af_r']){
                        $af_settings['af_enable_registration'] = 'on';
                    }

                    if($af_0_9['af_rp']){
                        $af_settings['af_enable_retrieve_password'] = 'on';
                    }
                }

                update_option('af_settings', $af_settings);
            }

        //return settings
        return $af_settings;
    }


    /**
     * Adds menu item to WordPress admin menu
     */
    public function constructMenu(){
        add_options_page(
                __('Login with email', 'login-af-email'),
                'Login with email',
                'manage_options',
                'login-af-email',
                array($this, 'smartSettings'));
    }

    /**
     * Renders the settings page
     */
    public function smartSettings(){
?>

        <div class="container-fluid alignleft" style="text-align:left;">
            <h2></h2>
            <form action="options.php" method="POST">
                <?php
                    settings_fields('af_settings');
                    do_settings_sections('login-af-email');
                ?>
                <input type="submit" class="button button-primary" value="<?php _e('Save', 'login-af-email'); ?>">
            </form>
        </div>

<?php
    }

    /**
     * Configures the Settings API
     */
    public function registerSettings(){
        register_setting('af_settings', 'af_settings');

        //
        // Enable Smartness Section
        //
        add_settings_section('af_enable',
                __('', 'login-af-email'),
                array($this, 'callback_afEnableSection'), 'login-af-email');

        //Enable in Login
        add_settings_field('af_enable_login',
                __('Login', 'login-af-email'),
                array($this, 'callback_afEnableLoginField'),
                'login-af-email',
                'af_enable');

        //Enable in Registration
        add_settings_field('af_enable_registration',
                __('Registration', 'login-af-email'),
                array($this, 'callback_afEnableRegistrationField'),
                'login-af-email',
                'af_enable');

        //Enable in Retrieve Password
        add_settings_field('af_enable_retrieve_password',
                __('Retrieve Password', 'login-af-email'),
                array($this, 'callback_afEnableRetrievePasswordField'),
                'login-af-email',
                'af_enable');


        //
        //Custom Message
        //
        add_settings_section('af_string',
                __('set message for users', 'login-af-email'),
                array($this, 'callback_afString'),
                'login-af-email');

        //Login: Empty Both Fields
        add_settings_field(self::STRING_LOG_EMPTY_BOTH_FIELDS,
                __('<span class="legend login">L</span> Both Fields Empty:', 'login-af-email'),
                array($this, 'callback_afStringLogEmptyBothFields'),
                'login-af-email',
                'af_string');

        //Login: Empty Email
        add_settings_field(self::STRING_LOG_EMPTY_EMAIL,
                __('<span class="legend login">L</span> Empty Email:', 'login-af-email'),
                array($this, 'callback_afStringLogEmptyEmail'),
                'login-af-email',
                'af_string');

        //Login: Invalid Email
        add_settings_field(self::STRING_LOG_INVALID_EMAIL,
                __('<span class="legend login">L</span> Invalid Email:', 'login-af-email'),
                array($this, 'callback_afStringLogInvalidEmail'),
                'login-af-email',
                'af_string');

        //Login: Empty Password
        add_settings_field(self::STRING_LOG_EMPTY_PASSWORD,
                __('<span class="legend login">L</span> Empty Password:', 'login-af-email'),
                array($this, 'callback_afStringLogEmptyPassword'),
                'login-af-email',
                'af_string');

        //Login: Invalid email or password
        add_settings_field(self::STRING_LOG_INVALID_EMAIL_PASSWORD,
                __('<span class="legend login">L</span> Invalid Email or Password:', 'login-af-email'),
                array($this, 'callback_afStringLogInvalidEmailPassword'),
                'login-af-email',
                'af_string');



        //Registration: Empty Email
        add_settings_field(self::STRING_REG_EMPTY_EMAIL,
                __('<span class="legend registration">R</span> Empty Email:', 'login-af-email'),
                array($this, 'callback_afStringRegEmptyEmail'),
                'login-af-email',
                'af_string');

        //Registration: Invalid Email
        add_settings_field(self::STRING_REG_INVALID_EMAIL,
                __('<span class="legend registration">R</span> Invalid Email:', 'login-af-email'),
                array($this, 'callback_afStringRegInvalidEmail'),
                'login-af-email',
                'af_string');

        //Registration: Registered Email
        add_settings_field(self::STRING_REG_REGISTERED_EMAIL,
                __('<span class="legend registration">R</span> Registered Email:', 'login-af-email'),
                array($this, 'callback_afStringRegRegisteredEmail'),
                'login-af-email',
                'af_string');

        //Registration: Registration Complete
        add_settings_field(self::STRING_REG_LOG_REGISTRATION_COMPLETE,
                __('<span class="legend registration">R</span> Registration Complete:', 'login-af-email'),
                array($this, 'callback_afStringRegLogRegistrationComplete'),
                'login-af-email',
                'af_string');



        //Retrieve Password: Empty Email
        add_settings_field(self::STRING_RP_EMPTY_EMAIL,
                __('<span class="legend retrieve-pass">RP</span> Empty Email:', 'login-af-email'),
                array($this, 'callback_afStringRpEmptyEmail'),
                'login-af-email',
                'af_string');

        //Retrieve Password: Invalid Email
        add_settings_field(self::STRING_RP_NO_USER_REGISTERED,
                __('<span class="legend retrieve-pass">RP</span> No User Registered:', 'login-af-email'),
                array($this, 'callback_afStringRpNoUserRegistered'),
                'login-af-email',
                'af_string');

        //Retrieve Password: Invalid Email
        add_settings_field(self::STRING_RP_INVALID_EMAIL,
                __('<span class="legend retrieve-pass">RP</span> Invalid Email:', 'login-af-email'),
                array($this, 'callback_afStringRpInvalidEmail'),
                'login-af-email',
                'af_string');

        //Retrieve Password: Notification
        add_settings_field(self::STRING_RP_NOTIFICATION,
                __('<span class="legend retrieve-pass">RP</span> Notification:', 'login-af-email'),
                array($this, 'callback_afStringRpNotification'),
                'login-af-email',
                'af_string');

        //Reset Password & Login: Check Email
        add_settings_field(self::STRING_RP_LOG_CHECK_EMAIL,
                __('<span class="legend retrieve-pass">RP</span> Check Email:', 'login-af-email'),
                array($this, 'callback_afStringRpLogCheckEmail'),
                'login-af-email',
                'af_string');
    }


    ############################################################################
    # Custom Message Section and Fields
    ############################################################################
    public function callback_afString(){
?>
        <div> <?php _e('You can also use HTML inside the fields...', 'login-af-email'); ?> </div><br>
        <div style="text-align:left;">
            <span class="legend login">L =</span> <?php _e('Login', 'login-af-email'); ?><br>
            <span class="legend registration">R = </span> <?php _e('Registration', 'login-af-email'); ?><br>
            <span class="legend retrieve-pass">RP = </span>  <?php _e('Retrieve Password', 'login-af-email'); ?>
        </div>
<?php
    }

    public function callback_afStringLogEmptyBothFields(){
        $this->callback_afStringField(self::STRING_LOG_EMPTY_BOTH_FIELDS);
    }

    public function callback_afStringLogEmptyEmail(){
        $this->callback_afStringField(self::STRING_LOG_EMPTY_EMAIL);
    }

    public function callback_afStringLogInvalidEmail(){
        $this->callback_afStringField(self::STRING_LOG_INVALID_EMAIL);
    }

    public function callback_afStringLogEmptyPassword(){
        $this->callback_afStringField(self::STRING_LOG_EMPTY_PASSWORD);
    }

    public function callback_afStringLogInvalidEmailPassword(){
        $this->callback_afStringField(self::STRING_LOG_INVALID_EMAIL_PASSWORD);
    }

    public function callback_afStringRegEmptyEmail(){
        $this->callback_afStringField(self::STRING_REG_EMPTY_EMAIL);
    }

    public function callback_afStringRegInvalidEmail(){
        $this->callback_afStringField(self::STRING_REG_INVALID_EMAIL);
    }

    public function callback_afStringRegRegisteredEmail(){
        $this->callback_afStringField(self::STRING_REG_REGISTERED_EMAIL);
    }

    public function callback_afStringRegLogRegistrationComplete(){
        $this->callback_afStringField(self::STRING_REG_LOG_REGISTRATION_COMPLETE);
    }

    public function callback_afStringRpEmptyEmail(){
        $this->callback_afStringField(self::STRING_RP_EMPTY_EMAIL);
    }

    public function callback_afStringRpNoUserRegistered(){
        $this->callback_afStringField(self::STRING_RP_NO_USER_REGISTERED);
    }

    public function callback_afStringRpInvalidEmail(){
        $this->callback_afStringField(self::STRING_RP_INVALID_EMAIL);
    }

    public function callback_afStringRpNotification(){
        $this->callback_afStringField(self::STRING_RP_NOTIFICATION);
    }

    public function callback_afStringRpLogCheckEmail(){
        $this->callback_afStringField(self::STRING_RP_LOG_CHECK_EMAIL);
    }

    public function callback_afStringField($string_key){
        $val = (!empty(self::$settings) && isset(self::$settings[$string_key]))?
                self::$settings[$string_key] : '';

?>
<input type="text" id="<?php echo $string_key ?>" name="af_settings[<?php echo $string_key ?>]"
       value="<?php echo esc_html($val); ?>" class="regular-text ltr">
<?php
    }

    ############################################################################
    # Enable Smartness Section and Fields
    ############################################################################
    public function callback_afEnableSection(){
        _e('Use email instead of username to', 'login-af-email');
    }

    public function callback_afEnableLoginField(){
?>
        <input type="checkbox" id="af_enable_login" name="af_settings[af_enable_login]" <?php echo (self::isLoginSmart())? 'checked="checked"':''; ?> >
<?php
    }

    public function callback_afEnableRegistrationField(){
?>
        <input type="checkbox" id="af_enable_registration" name="af_settings[af_enable_registration]" <?php echo (self::isRegistrationSmart())? 'checked="checked"':''; ?> >
<?php
    }

    public function callback_afEnableRetrievePasswordField(){
?>
        <input type="checkbox" id="af_enable_retrieve_password" name="af_settings[af_enable_retrieve_password]" <?php echo (self::isRetrievePasswordSmart())? 'checked="checked"':''; ?> >
<?php
    }


    ############################################################################
    # Getters
    ############################################################################

    /**
     * Wheather Email enabled login or not
     * @return type
     */
    public static function isLoginSmart(){
        //enable for login or not
        $val = (!empty(self::$settings) && isset(self::$settings['af_enable_login']));
        return $val;
    }

    /**
     * Wheather Email enabled registration or not
     * @return type
     */
    public static function isRegistrationSmart(){
        //enable for login or not
        $val = (!empty(self::$settings) && isset(self::$settings['af_enable_registration']));
        return $val;
    }

    /**
     * Wheather Email enabled retrieve password or not
     * @return type
     */
    public static function isRetrievePasswordSmart(){
        //enable for login or not
        $val = (!empty(self::$settings) && isset(self::$settings['af_enable_retrieve_password']));
        return $val;
    }

    /**
     * Returns custom strings saved by user.
     */
    public static function getString($string_key){
        $val = (!empty(self::$settings) && isset(self::$settings[$string_key]))?
                self::$settings[$string_key] : '';

        return $val;
    }

    ############################################################################
    # Misc
    ############################################################################

    public function adminEnqueueStyle($hook){
        wp_register_style('af_admin_style', af_URL.'login/adminstyles.css');
        wp_enqueue_style('af_admin_style');
    }
}
