<?php
/***************************************
 * scripts and enqueueing for this theme
 ***************************************/

// Hide the editor on a page with a specific page template
add_action( 'admin_head', 'hide_editor' );
function hide_editor() {
	$template_file = $template_file = basename( get_page_template() );
	if($template_file == 'custompage.php'){
		remove_post_type_support('page', 'editor');
	}
}

// hide admin bar for non-admin's --show only for admin and editor roles
function affari_hide_admin_bar_settings()
{
?>
	<style type="text/css">
		.show-admin-bar {
			display: none;
		}
	</style>
<?php
}
function affari_disable_admin_bar() {
   if (current_user_can('administrator') || current_user_can('editor') ) {
     show_admin_bar(true);
   } else {
     show_admin_bar(false);
   }
}
add_action('after_setup_theme', 'affari_disable_admin_bar');

// redirect logout to home page and allow logout without confirmation
add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}
add_action('check_admin_referer', 'logout_without_confirm', 10, 2);
function logout_without_confirm($action, $result)
{
    if ($action == "log-out" && !isset($_GET['_wpnonce'])) {
        $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : 'url-you-want-to-redirect';
        $location = str_replace('&amp;', '&', wp_logout_url($redirect_to));
        header("Location: $location");
        die;
    }
}
// redirect logout to home page and allow logout without confirmation END


//// CUSTOM ADMIN PANEL DASHBOARD

//remove Profile set admin skin -- comment/uncomment next line to enable/disable
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

//remove wordpress stuff from admin menu
function new_menu_remove() {
        global $wp_admin_bar;
        /* Remove their stuff */
        $wp_admin_bar->remove_menu('new-content');
}

add_action('wp_before_admin_bar_render', 'new_menu_remove', 0);
//remove wordpress stuff from admin menu end

//remove wordpress stuff from admin menu
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'edit' );
}
add_action( 'wp_before_admin_bar_render', 'wpse200296_before_admin_bar_render' );

function wpse200296_before_admin_bar_render()
{
    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('customize');
}
function admin_bar_logo_remove() {
        global $wp_admin_bar;
        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
}

add_action('wp_before_admin_bar_render', 'admin_bar_logo_remove', 0);
//remove wordpress stuff from admin menu end

// hide administration page header logo and blavatar
function remove_admin_logo() {
echo '<style>
#wp-admin-bar-wp-logo{ display: none; }
img.blavatar { display: none;}
#wpadminbar .quicklinks li div.blavatar {display:none;}
#wpadminbar .quicklinks li .blavatar {display:none;}
/*#wpadminbar #wp-admin-bar-new-content .ab-icon:before {display:none;}*/
#wpadminbar .quicklinks li .blavatar:before {display:none;}
</style>';
}
add_action('admin_head', 'remove_admin_logo');
// hide administration page header logo and blavatar end

// remove wp help and branding menu
function aff_hide_help() {
    echo '<style type="text/css">#contextual-help-link-wrap { display: none !important; }</style>';
}
add_action('admin_head', 'aff_hide_help');

function remove_aff_branding_menu(){
  //remove_submenu_page( $menu_slug, $submenu_slug );
  remove_submenu_page( 'plugins.php', 'remove-wp-branding' ); //this worked for eliminating the item from the menu
}
add_action( 'admin_menu', 'remove_aff_branding_menu' );
// remove wp help and branding menu end



//// AFFARI CUSTOM LOGIN

// custom error messages
function my_login_custom_css() {
echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/login/custom-login-styles.css" />'; }
add_action( 'login_enqueue_scripts', 'my_login_custom_css' );


// custom LOGIN screen
function my_custom_login() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-styles.css" />';
}
add_action('login_head', 'my_custom_login');

function insert_admin_stylesheet() {
        wp_register_style( 'admin_style', get_template_directory_uri(). '/login/adminstyles.css', false, '1.0.0' );
        wp_enqueue_style( 'admin_style' );
}
add_action( 'admin_enqueue_scripts', 'insert_admin_stylesheet' );

add_filter( 'login_headerurl', 'affari_loginlogo_url' );
function affari_loginlogo_url($url)
{
  return home_url();
}
add_filter( 'login_headerurl', 'affari_loginlogo_url' );
function loginlogo_url($url)
{
  return home_url();
}

// end custom LOGIN screen

// change wp howdy
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );
 function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );
 if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __(' %1$s'), $current_user->user_firstname . ' ' . $current_user->user_lastname );
$class = empty( $avatar ) ? '' : 'with-avatar';
 $wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );
}
}
// end change howdy

// Admin footer modification
// link == comment to disable
// admin footer modification version msg edit
function remove_footer_admin () {
    echo '<span id="footer-thankyou">Developed by <a href="https://www.affarimedia.com" target="_blank">Affari Media</a> - </span>';
}
add_filter('admin_footer_text', 'remove_footer_admin');
// end version msg edit
// version msg edit
function wpse_edit_footer() { add_filter( 'admin_footer_text', 'wpse_edit_text', 11 ); }
function wpse_edit_text($content) { return wp_title(); }
add_action( 'admin_init', 'wpse_edit_footer' );
function my_footer_shh() { remove_filter( 'update_footer', 'core_update_footer' ); }
add_action( 'admin_menu', 'my_footer_shh' );
// end version msg edit

// error login messages override
function login_error_override() { return 'Incorrect login details.'; }
add_filter('login_errors', 'login_error_override');
function my_login_head() {remove_action('login_head', 'wp_shake_js', 12); }
add_action('login_head', 'my_login_head');
// end login style

// remove welcome message
add_action( 'wp_dashboard_setup', 'remove_welcome_panel' );
function remove_welcome_panel() {
    global $wp_filter;
    unset( $wp_filter['welcome_panel'] );
}
// end remove welcome message

//// END AFFARI CUSTOM LOGIN


function affari_scripts_and_styles() {

	global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

	if ( ! is_admin() ) {
        // register main stylesheet
		wp_register_style( 'affari-stylesheet', get_stylesheet_directory_uri() . '/dist/style.css', array(), '', 'all' );

		wp_enqueue_script( 'bundle', get_stylesheet_directory_uri() . '/dist/bundle.js', '', '', true );
		wp_enqueue_style( 'affari-stylesheet' );
	}
}

function affari_custom_image_sizes()
{
	add_image_size('hero-image-x1', 1600, 800, false);
}

/**
* Remove junk from head
*/
function affari_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'affari_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'affari_remove_wp_ver_css_js', 9999 );

}

function rw_title( $title, $sep, $seplocation ) {
	global $page, $paged;

	// Don't affect in feeds.
	if ( is_feed() ) {
		return $title;
	}

	// Add the blog's name
	if ( 'right' == $seplocation ) {
		$title .= get_bloginfo( 'name' );
	} else {
		$title = get_bloginfo( 'name' ) . $title;
	}

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " {$sep} {$site_description}";
	}

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 ) {
		$title .= " {$sep} " . sprintf( __( 'Page %s', 'dbt' ), max( $paged, $page ) );
	}

	return $title;

}

function affari_rss_version() {
	return '';
}

function affari_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) ) {
		$src = remove_query_arg( 'ver', $src );
	}

	return $src;
}

function affari_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

function affari_remove_recent_comments_style() {
	global $wp_widget_factory;
	if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
		remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
	}
}

function affari_gallery_style( $css ) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}

function affari_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	// default thumb size
	set_post_thumbnail_size( 125, 125, true );

	// wp custom background (thx to @bransonwerner for update)
	add_theme_support( 'custom-background',
		array(
			'default-image'          => '',    // background image default
			'default-color'          => '',    // background color default (dont add the #)
			'wp-head-callback'       => '_custom_background_cb',
			'admin-head-callback'    => '',
			'admin-preview-callback' => ''
		)
	);

	// rss thingy
	add_theme_support( 'automatic-feed-links' );

	// to add header image support go here: https://www.wpbeginner.com/beginners-guide/how-to-add-a-background-image-in-wordpress/

	// adding post format support
	add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	);

	// wp menus
	add_theme_support( 'menus' );

	// registering wp3+ menus
	register_nav_menus(
		array(
			'main-nav'     => __( 'The Main Menu', 'affari' ),   // main nav in header
			'footer-links' => __( 'Footer Links', 'affari' ) // secondary nav in footer
		)
	);

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form'
	) );

}

function affari_page_navi() {
	global $wp_query;
	$bignum = 999999999;
	if ( $wp_query->max_num_pages <= 1 ) {
		return;
	}
	echo '<nav class="pagination">';
	echo paginate_links( array(
		'base'      => str_replace( $bignum, '%#%', esc_url( get_pagenum_link( $bignum ) ) ),
		'format'    => '',
		'current'   => max( 1, get_query_var( 'paged' ) ),
		'total'     => $wp_query->max_num_pages,
		'prev_text' => '&larr;',
		'next_text' => '&rarr;',
		'type'      => 'list',
		'end_size'  => 3,
		'mid_size'  => 3
	) );
	echo '</nav>';
}

function affari_filter_ptags_on_images( $content ) {
	return preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
}
