<?php

function aff_admin_bar_theme_style() {

  if(get_option( 'aff_primary_color') != ""):
    $aff_primary    = get_option( 'aff_primary_color');
  else :
    $aff_primary    = "#3498db";
  endif;
  if(get_option( 'aff_secondary_color') != ""):
    $aff_secondary  = get_option( 'aff_secondary_color');
  else :
    $aff_secondary    = "#2581bf";
  endif;

  wp_enqueue_style(
		'aff-admin-bar-style',
		get_template_directory_uri() . '/login/aff_styles_adminbar.css'
	);

  $admin_bar_css = "

    #wpadminbar {
      background: {$aff_primary};
    }

    #wpadminbar .menupop .ab-sub-wrapper,#wpadminbar .shortlink-input {
      background: {$aff_primary};
    }

  ";
  wp_add_inline_style( 'aff-admin-bar-style', $admin_bar_css );
}
add_action( 'admin_enqueue_scripts', 'aff_admin_bar_theme_style' );

function aff_admin_theme_style() {

  if(get_option( 'aff_primary_color') != ""):
    $aff_primary    = get_option( 'aff_primary_color');
  else :
    $aff_primary    = "#3498db";
  endif;
  if(get_option( 'aff_secondary_color') != ""):
    $aff_secondary  = get_option( 'aff_secondary_color');
  else :
    $aff_secondary    = "#2581bf";
  endif;

  wp_enqueue_style(
		'aff-admin-style',
		get_template_directory_uri() . '/login/aff_styles_admin.css'
	);

  $admin_css = "

    a,
    input[type=checkbox]:checked:before,
    .view-switch a.current:before {
      color: {$aff_primary}
    }

    a:hover {
      color: {$aff_secondary}
    }

    #adminmenu li a:focus div.wp-menu-image:before,#adminmenu li.opensub div.wp-menu-image:before,#adminmenu li:hover div.wp-menu-image:before {
      color:  {$aff_primary}!important;
    }

    #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head,#adminmenu .wp-menu-arrow,#adminmenu .wp-menu-arrow div,#adminmenu li.current a.menu-top,#adminmenu li.wp-has-current-submenu a.wp-has-current-submenu,.folded #adminmenu li.current.menu-top,.folded #adminmenu li.wp-has-current-submenu,/* Hover actions */
    #adminmenu li.menu-top:hover,#adminmenu li.opensub>a.menu-top,#adminmenu li>a.menu-top:focus {
      background: {$aff_primary};
      background:#FFF
    }

    #adminmenu .opensub .wp-submenu li.current a,#adminmenu .wp-submenu li.current,#adminmenu .wp-submenu li.current a,#adminmenu .wp-submenu li.current a:focus,#adminmenu .wp-submenu li.current a:hover,#adminmenu a.wp-has-current-submenu:focus+.wp-submenu li.current a,#adminmenu .wp-submenu .wp-submenu-head,/* Dashicons */
    #adminmenu .current div.wp-menu-image:before,#adminmenu .wp-has-current-submenu div.wp-menu-image:before,#adminmenu a.current:hover div.wp-menu-image:before,#adminmenu a.wp-has-current-submenu:hover div.wp-menu-image:before,#adminmenu li.wp-has-current-submenu:hover div.wp-menu-image:before, #adminmenu li:hover div.wp-menu-image:before {
      color: {$aff_primary}
    }

    #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu div.wp-menu-name {
      color: {$aff_primary}
    }

    .wrap .add-new-h2,.wrap .add-new-h2:active {
      background: {$aff_primary};
    }

    .wrap .add-new-h2:hover {
      background: {$aff_secondary}
    }

    div.updated {
      border-left: 5px solid  {$aff_primary};
    }

    #adminmenu li a.wp-has-current-submenu .update-plugins,
    #adminmenu li.current a .awaiting-mod {
      background-color: {$aff_primary};
    }

    .wp-core-ui .button:hover,.wp-core-ui .button-secondary:hover,.wp-core-ui .button-primary {
      background: {$aff_primary};
    }

    .wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover {
      background: {$aff_secondary};
    }

    .composer-switch a,.composer-switch a:visited,.composer-switch a.wpb_switch-to-front-composer,.composer-switch a:visited.wpb_switch-to-front-composer,.composer-switch .logo-icon {
      background-color: {$aff_primary}!important
    }

    .composer-switch .vc-spacer, .composer-switch a.wpb_switch-to-composer:hover, .composer-switch a:visited.wpb_switch-to-composer:hover, .composer-switch a.wpb_switch-to-front-composer:hover, .composer-switch a:visited.wpb_switch-to-front-composer:hover {
      background-color:  {$aff_secondary}!important
    }

  ";
  wp_add_inline_style( 'aff-admin-style', $admin_css );
}
add_action( 'admin_enqueue_scripts', 'aff_admin_theme_style' );

// Update Admin Footer
function aff_swap_footer_admin() {
  echo '';
}
add_filter( 'admin_footer_text', 'aff_swap_footer_admin' );

// Remove deafflt HTML height on the admin bar callback
function fui_admin_bar_style() {
  if ( is_admin_bar_showing() ) {
?>
  <style type="text/css" media="screen">
    html { margin-top: 46px !important; }
    * html body { margin-top: 46px !important; }
  </style>
<?php } }
add_theme_support( 'admin-bar', array( 'callback' => 'fui_admin_bar_style' ) );


// create option on Setting page

add_action( 'admin_enqueue_scripts', 'mw_enqueue_color_picker' );
function mw_enqueue_color_picker( $hook_suffix ) {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'my-script-handle', get_template_directory_uri('', __FILE__ ), array( 'wp-color-picker' ), false, true );
}

// Customize affari Admin UI Colors
$aff_color_settings = new aff_color_settings();
class aff_color_settings {
    function __construct() {
        add_filter( 'admin_init' , array( &$this , 'register_fields' ) );
    }
    function register_fields() {
        register_setting( 'general', 'aff_primary_color', 'esc_attr' );
        add_settings_field('aff_primary_color', '<label for="aff_primary_color">'.__('Admin Primary Color:' , 'aff_primary_color' ).'</label>' , array(&$this, 'fields_html') , 'general' );
    }
    function fields_html() {
        $value = get_option( 'aff_primary_color', '' );
        echo '<input type="text" id="aff_primary_color" name="aff_primary_color" value="' . $value . '" data-deafflt-color="#3498db" />';
        echo "
          <script>
            jQuery(document).ready(function($){
              $('#aff_primary_color').wpColorPicker();
            });
          </script>
          ";
    }
}

$aff_secondary_color_settings = new aff_secondary_color_settings();
class aff_secondary_color_settings {
    function __construct() {
        add_filter( 'admin_init' , array( &$this , 'register_fields' ) );
    }
    function register_fields() {
        register_setting( 'general', 'aff_secondary_color', 'esc_attr' );
        add_settings_field('aff_secondary_color', '<label for="aff_secondary_color">'.__('Admin Secondary Color:' , 'aff_secondary_color' ).'</label>' , array(&$this, 'fields_html') , 'general' );
    }
    function fields_html() {
        $value = get_option( 'aff_secondary_color', '' );
        echo '<input type="text" id="aff_secondary_color" name="aff_secondary_color" value="' . $value . '" data-deafflt-color="#2581bf" />';
        echo "
          <script>
            jQuery(document).ready(function($){
              $('#aff_secondary_color').wpColorPicker();
            });
          </script>
          ";
    }
}
