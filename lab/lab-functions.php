<?php
/**
 * 'functions.php' extras
 * This file is used for all utility functions.
 * Please place all add_action calls, with their relevant functions within lab-functions.php
 */

 // hide ACF from non ADMIN
 // change this section to: add_filter('acf/settings/show_admin', '__return_false');
 // for hide for everyone
  add_filter('acf/settings/show_admin', 'my_acf_show_admin');
  function my_acf_show_admin( $show ) {
      return current_user_can('manage_options');
  }
  // Drastically speed up the load times of the post edit page!
  add_filter('acf/settings/remove_wp_meta_box', '__return_true');

/**
 * Loops through the flexible content field and displays any partials that have templates within /partials/
 */
function affari_display_flexible_content()
{
	global $post;

	if ( have_rows('flexible_content', $post->ID) ) {
		while ( have_rows('flexible_content', $post->ID ) ) {
			the_row();

			$fileName = __DIR__ . '/../template-parts/' . get_row_layout() . '.php';

			if ( file_exists($fileName) ) {
				include($fileName);
			}
		}
	}
}

/** display Portfolios */
function affari_display_portfolios()
{
	global $post;

	if ( have_rows('portfolio_posts_af', $post->ID) ) {
		while ( have_rows('portfolio_posts_af', $post->ID ) ) {
			the_row();

			$fileName = __DIR__ . '/../template-parts/' . get_row_layout() . '.php';

			if ( file_exists($fileName) ) {
				include($fileName);
			}
		}
	}
}

function affari_display_hero_image_room()
{
	include(__DIR__ . '/../rooms/hero-image-room.php');
}

/**
 * Includes the hero image / videos / gallery
 */
function affari_display_hero_section()
{
	include(__DIR__ . '/../template-parts/hero-image.php');
}

function affari_display_hero_video()
{
	include(__DIR__ . '/../template-parts/hero-video.php');
}

function affari_display_hero_gallery()
{
	include(__DIR__ . '/../template-parts/hero-image-gallery.php');
}

/** embed video full for hero video */

function affari_function_embed_video ( $html ) {

  return '<div class="heropanel--video">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'affari_function_embed_video ', 10, 3 );
add_filter( 'video_embed_html', 'affari_function_embed_video ' );

// /** affari Portfolio option in admin menu --fixed on Admin menu after posts */
//
// function affari_post_type() {
//   register_post_type( 'portfolio',
//     array(
//       'labels' => array(
//         'name' => __( 'Portfolio`s' ),
//         'singular_name'         => __( 'Portfolio' ),
//         'all_items'             => 'All Portfolio`s',
//         'add_new_item'          => 'Add New Portfolio',
//         'add_new'               => 'Add Portfolio page',
//         'new_item'              => 'New Portfolio',
//         'edit_item'             => 'Edit Portfolio',
//         'update_item'           => 'Update Portfolio',
//         'view_item'             => 'View Portfolio',
//         'view_items'            => 'View Portfolio`s',
//         'search_items'          => 'Search for Portfolio`s'
//       ),
//       'public'                  => true,
//       'has_archive'             => true,
//       'rewrite'                 => true,
//       'menu_position'           => 2,
//       'taxonomies' => array('category'),
//       'show_ui' => true,
//       'exclude_from_search' => true,
//       'hierarchical' => true,
//       'supports' => array( 'title', 'editor', 'thumbnail' ),
//       'query_var' => true,
//       'menu_icon'               => 'dashicons-portfolio',
//     )
//   );
// }
// add_action( 'init', 'affari_post_type' );
// flush_rewrite_rules( false );

// change 'enter title here'
function wpb_change_title_text( $title ){
     $screen = get_current_screen();
     if  ( 'portfolio' == $screen->post_type ) {
          $title = 'Enter Portfolio name (this will be the name of the page too)';
     }
     return $title;
}
add_filter( 'enter_title_here', 'wpb_change_title_text' );

/**
 * Alternative to the_excerpt.
 *
 * Pass any string through this function along with a maximum character limit
 *
 * @param $string string - No HTML, use strip_tags() on any HTML content prior to using this function
 * @param $your_desired_width int - The desired width in characters, how strict this is maintained is controllable via the $strict argument
 * @param $strict bool - default true
 * How strict the character limit should be. If set to true, this will remove any words (even if in the middle of a word) that exceed the character limit.
 * If set to false, this will allow a word to go over the character limit, if the character limit ended in the middle of that word
 *
 * Example: "Hello World" has 11 characters (1 space, 10 letters). If the character limit was set to 9, strict would behave in the following way:
 *
 *     True: would return "Hello..."
 *     False: Would Return "Hello World" Because 9 characters ends on the 'r' of World, it will complete the word and THEN end.
 *
 * @return bool|string
 */
function affari_truncate_string( $string, $your_desired_width, $strict = true ) {
	$parts       = preg_split( '/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE );
	$parts_count = count( $parts );

	$greater = false;

	$startingLength = strlen($string);
	$length    = 0;
	$last_part = 0;

	for ( ; $last_part < $parts_count; ++ $last_part ) {
		$length += strlen( $parts[ $last_part ] );
		if ( $length > $your_desired_width ) {
			$greater = true;
			if ( !$strict ) {
				$last_part++;
			}
			break;
		}
	}

  if ( $startingLength === $length ) {
		$greater = false;
	}

	$finalString = implode( array_slice( $parts, 0, $last_part ) );
	$finalChar   = substr( $finalString, - 1 );

	while ( $finalChar === ',' || $finalChar === ' ' || $finalChar === '.' ) {
		$finalString = substr( $finalString, 0, - 1 );
		$finalChar   = substr( $finalString, - 1 );
	}

	return $greater ? $finalString . '...' : $finalString;
}
