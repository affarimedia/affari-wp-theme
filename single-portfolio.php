<?php
if (!is_user_logged_in() ) {
wp_redirect( '/portfolio-login' . '?redirect_to=' . $_SERVER["REQUEST_URI"] );
exit;
}
?>

<?php get_header(); ?>

    <section id="portfolio">

        <div class="container">

<h1 align="center"> https://affarilab.com<?php echo $_SERVER['REQUEST_URI'] ?></h1>

          <?php the_field( 'portfolio_video' ); ?><br>
          <p>Client: <?php the_field( 'portfolio_client' ); ?><br><br>
          Year: <?php the_field( 'portfolio_year' ); ?><br><br>
          Process:<br><?php the_field( 'portfolio_process' ); ?><br><br>
          Deliverables:<br><?php the_field( 'portfolio_deliverables' ); ?><br><br>
          <h3><?php the_field( 'portfolio_title' ); ?></h3>
          <?php the_field( 'portfolio_description' ); ?><br>

          <?php $portfolio_image = get_field( 'portfolio_image' ); ?>
          <?php if ( $portfolio_image ) { ?>
          	<img src="<?php echo $portfolio_image['url']; ?>" alt="<?php echo $portfolio_image['alt']; ?>" />
          <?php } ?>
          <?php if ( empty($portfolio_image) ) { ?>
          <img src="<?php echo the_field('portfolio_image_url'); ?>" />
          <?php } ?>
        </p>

        </div>

    </section>

<?php get_footer(); ?>
